package Utils;

import java.util.*;

import Classes.Dataset;
import Handler.CombinationHandler;

//Tao 1 mang 1000 so ngau nhien
//Tim kiem x bang 2 thread

public class TimKiem extends Thread {
	public Dataset dataset;
	public boolean isExisted = false;
	@Override
	public void run(){
		timNuaDau.start();
		try {
			timNuaDau.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timNuaSau.start();
		try {
			timNuaSau.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	Thread timNuaDau = new Thread(new Runnable() {
		@Override
		public void run() {
			for(int i = 0; i< (CombinationHandler.finalDataset).size()/2; i++)
			{
				if (dataset.getX().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getX()) == 0 &&
						dataset.getY().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getY()) == 0 &&
								dataset.getZ().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getZ()) == 0)
				{
					isExisted = true;
					timNuaDau.stop();
					timNuaSau.stop();
					break;
				}				
			}			
		}
	});
	Thread timNuaSau = new Thread(new Runnable() {
		@Override
		public void run() {
			for(int i = (CombinationHandler.finalDataset).size() / 2; i< (CombinationHandler.finalDataset).size(); i++)
			{
				if (dataset.getX().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getX()) == 0 &&
						dataset.getY().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getY()) == 0 &&
							dataset.getZ().compareToIgnoreCase((CombinationHandler.finalDataset).get(i).getZ()) == 0)
				{
					isExisted = true;
					timNuaDau.stop();
					timNuaSau.stop();
					break;
				}				
			}			
		}
	});
//	public TimKiem() {
//		mang = new int[100];
//		Random rand = new Random();
//		x = 30;
//		vitri = -1;
//		for (int i = 0; i < mang.length; i++) {
//			mang[i] = rand.nextInt(100);
//			//mang[i] = i;
//		}
//	}

	public synchronized boolean getResult() {
		return isExisted;
	}
}
