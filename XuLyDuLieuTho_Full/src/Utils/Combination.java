package Utils;

//Java program to print all combination of size r in an array of size n 
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.ResolveResult;

import Classes.Dataset; 

public class Combination {
	
	private static List<Dataset> result = new ArrayList<>();

	/* arr[] ---> Input Array 
	data[] ---> Temporary array to store current combination 
	start & end ---> Staring and Ending indexes in arr[] 
	index ---> Current index in data[] 
	r ---> Size of a combination to be printed */
	public static void combinationUtil(int arr[], int data[], int start, 
								int end, int index, int r) 
	{ 
		// Current combination is ready to be printed, print it 
		if (index == r) 
		{ 
			System.out.print("+1 ");
			for (int j=0; j<r; j++) {
				System.out.print((j+1) + ":" + data[j]+" "); 
			}
				
			System.out.println(""); 
			return; 
		} 

		// replace index with all possible elements. The condition 
		// "end-i+1 >= r-index" makes sure that including one element 
		// at index will make a combination with remaining elements 
		// at remaining positions 
		for (int i=start; i<=end && end-i+1 >= r-index; i++) 
		{ 
			data[index] = arr[i]; 
			combinationUtil(arr, data, i+1, end, index+1, r); 
		} 
	} 
	
	public static List<Dataset> combinationToDataset(int arr[], int data[], int start, 
			int end, int index, int r) 
	{
		Dataset temp = new Dataset();
		// Current combination is ready to be printed, print it 
		if (index == r) 
		{ 
			temp.setPrefix("+1");
			for (int j=0; j<r; j++) {
				switch (j) {
				case 0:
					temp.setX(String.valueOf(data[j]));
					break;
				case 1:
					temp.setY(String.valueOf(data[j]));
					break;
				case 2:
					temp.setZ(String.valueOf(data[j]));
					break;
				default:
					break;
				}
			}
			result.add(temp);
			return result;
		} 
		
		// replace index with all possible elements. The condition 
		// "end-i+1 >= r-index" makes sure that including one element 
		// at index will make a combination with remaining elements 
		// at remaining positions 
		for (int i=start; i<=end && end-i+1 >= r-index; i++) 
		{ 
			data[index] = arr[i]; 
			combinationToDataset(arr, data, i+1, end, index+1, r); 
		} 
		return result;
	} 
	
	
	
	// The main function that prints all combinations of size r 
	// in arr[] of size n. This function mainly uses combinationUtil() 
	public static void printCombination(int arr[], int n, int r) 
	{ 
		// A temporary array to store all combination one by one 
		int data[]=new int[r]; 

		// Print all combination using temprary array 'data[]' 
		combinationUtil(arr, data, 0, n-1, 0, r); 
	} 
	
	public static List<Dataset> generateCombination(int arr[], int n, int r) 
	{ 
		// A temporary array to store all combination one by one 
		int data[]=new int[r]; 

		// Print all combination using temprary array 'data[]' 
		return combinationToDataset(arr, data, 0, n-1, 0, r); 
	} 
} 

/* This code is contributed by Devesh Agrawal */

