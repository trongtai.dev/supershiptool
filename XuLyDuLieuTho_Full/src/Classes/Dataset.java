package Classes;

public class Dataset {
	private String prefix;
	private String x;
	private String y;
	private String z;
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public String getZ() {
		return z;
	}

	public void setZ(String z) {
		this.z = z;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return prefix + " 1:" + x + " 2:" + y + " 3:" + z;
	}
	
}
