package Classes;

public class Event {
	private int event_id;
	private int ACCOUNT_ID;
	private String TIMESTAMP;
	private int STATUS_CODE;
	private double latitude;
	private double longitude;
	private int ALTITUDE;
	private int device_id;
	private int driver_id;
	private double speed;
	private int FUEL_LEVEL;
	private int FUEL_TOTAL;
	private int COURSE;
	private int RUNTIME;
	private int DAY_RUNTIME;
	private int DURATION;
	private int SPEED_LIMIT;
	private int TYPE;
	private int PLATFORM;
	
	public int getEvent_id() {
		return event_id;
	}
	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}
	public int getACCOUNT_ID() {
		return ACCOUNT_ID;
	}
	public void setACCOUNT_ID(int aCCOUNT_ID) {
		ACCOUNT_ID = aCCOUNT_ID;
	}
	public String getTIMESTAMP() {
		return TIMESTAMP;
	}
	public void setTIMESTAMP(String tIMESTAMP) {
		TIMESTAMP = tIMESTAMP;
	}
	public int getSTATUS_CODE() {
		return STATUS_CODE;
	}
	public void setSTATUS_CODE(int sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getALTITUDE() {
		return ALTITUDE;
	}
	public void setALTITUDE(int aLTITUDE) {
		ALTITUDE = aLTITUDE;
	}
	public int getDevice_id() {
		return device_id;
	}
	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}
	public int getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(int driver_id) {
		this.driver_id = driver_id;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public int getFUEL_LEVEL() {
		return FUEL_LEVEL;
	}
	public void setFUEL_LEVEL(int fUEL_LEVEL) {
		FUEL_LEVEL = fUEL_LEVEL;
	}
	public int getFUEL_TOTAL() {
		return FUEL_TOTAL;
	}
	public void setFUEL_TOTAL(int fUEL_TOTAL) {
		FUEL_TOTAL = fUEL_TOTAL;
	}
	public int getCOURSE() {
		return COURSE;
	}
	public void setCOURSE(int cOURSE) {
		COURSE = cOURSE;
	}
	public int getRUNTIME() {
		return RUNTIME;
	}
	public void setRUNTIME(int rUNTIME) {
		RUNTIME = rUNTIME;
	}
	public int getDAY_RUNTIME() {
		return DAY_RUNTIME;
	}
	public void setDAY_RUNTIME(int dAY_RUNTIME) {
		DAY_RUNTIME = dAY_RUNTIME;
	}
	public int getDURATION() {
		return DURATION;
	}
	public void setDURATION(int dURATION) {
		DURATION = dURATION;
	}
	public int getSPEED_LIMIT() {
		return SPEED_LIMIT;
	}
	public void setSPEED_LIMIT(int sPEED_LIMIT) {
		SPEED_LIMIT = sPEED_LIMIT;
	}
	public int getTYPE() {
		return TYPE;
	}
	public void setTYPE(int tYPE) {
		TYPE = tYPE;
	}
	public int getPLATFORM() {
		return PLATFORM;
	}
	public void setPLATFORM(int pLATFORM) {
		PLATFORM = pLATFORM;
	}
}
