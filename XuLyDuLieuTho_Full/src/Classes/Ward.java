package Classes;

public class Ward {
    private int id;
    private String name;
    private String district;
    private String encodedpoly;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getEncodedpoly() {
        return encodedpoly;
    }

    public void setEncodedpoly(String encodedpoly) {
        this.encodedpoly = encodedpoly;
    }
    
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return this.id + " - " + this.name + " - " + this.district;
    }
}
