package Classes;

public class Frequency {
	private Ward ward;
	private int times;
	
	public Frequency(Ward ward) {
		this.ward = ward;
		this.times = 1;
	}
	
	public Ward getWard() {
		return ward;
	}
	public void setWard(Ward ward) {
		this.ward = ward;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.ward.toString() + " - " + times;
	}
}
