package Classes;


import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

/** A place on Earth, represented by a latitude/longitude pair. */
public class LatLng implements Serializable {

  private static final long serialVersionUID = 1L;

  public final double latitude;

  public final double longitude;

  /**
   * Constructs a location with a latitude/longitude pair.
   *
   * @param lat The latitude of this location.
   * @param lng The longitude of this location.
   */
  public LatLng( double var1,  double var3) {
      if (-180.0D <= var3 && var3 < 180.0D) {
          this.longitude = var3;
      } else {
          this.longitude = ((var3 - 180.0D) % 360.0D + 360.0D) % 360.0D - 180.0D;
      }

      this.latitude = Math.max(-90.0D, Math.min(90.0D, var1));
  }
  
  public final boolean equals(Object var1) {
      if (this == var1) {
          return true;
      } else if (!(var1 instanceof LatLng)) {
          return false;
      } else {
          LatLng var2 = (LatLng)var1;
          return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(var2.latitude) && Double.doubleToLongBits(this.longitude) == Double.doubleToLongBits(var2.longitude);
      }
  }

  public final String toString() {
      double var1 = this.latitude;
      double var3 = this.longitude;
      return (new StringBuilder(60)).append("lat/lng: (").append(var1).append(",").append(var3).append(")").toString();
  }
}