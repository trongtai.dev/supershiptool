package Classes;

public class FrequencyDataset {
	private Dataset dataset;
	private int times;
	
	public FrequencyDataset() {
	}
	
	public FrequencyDataset(Dataset dataset, int times) {
		this.dataset = dataset;
		this.times = times;
	}
	
	public FrequencyDataset(Dataset dataset) {
		this.dataset = dataset;
		this.times = 1;
	}
	
	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.dataset.toString() + " " + times;
	}
}
