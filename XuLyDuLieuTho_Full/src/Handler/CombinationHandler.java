package Handler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;

import Classes.Dataset;
import Classes.LatLng;
import Classes.Ward;
import Utils.Combination;
import Utils.PolyUtils;
import Utils.TimKiem;

public class CombinationHandler {

	//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<Ward> wards  = new ArrayList<>();
		
		//Danh sách các ward mà route hiện tại đi qua
		public static List<Ward> related_wards = new ArrayList<>();
		
		//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<String> routes  = new ArrayList<>();
		
		//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<Dataset> dataset  = new ArrayList<>();
		
		//Chứa dataset tổng hợp được từ tất cả các route
		public static List<Dataset> finalDataset = new ArrayList<>();
		
		//Chứa những dataset đã tồn tại ít nhất 1 lần trong finalDataset
		public static List<Dataset> existDataset = new ArrayList<>();
		
		public static int[] ward_id;
		
		public static long beginTimestamp = 0;
		
		public static long diffTimestamp = 0;
		
		public static Calendar cal;
		
	/*Driver function to check for above function*/
		
	public static void handler(String route) {
		System.out.println("Lộ trình: " + route);
		//System.out.println("Đi qua: ");
		for (Ward pt : wards){
            //Chuyển ward từ chuỗi poly thành List<LatLng>
            List<LatLng> wardPath = PolyUtils.decode(pt.getEncodedpoly());
            
            //Duyệt tất cả điểm của chuỗi poly xem có nằm trong ward hay không?
            for (LatLng point : PolyUtils.decode(route)){
                if(PolyUtils.containsLocation(point, wardPath, false)){
                    //Nếu đúng - Điểm nằm trong ward => Route đi qua ward
                    if(!related_wards.contains(pt)){
                    	//Chỉ thêm vào một lần duy nhất
                    	related_wards.add(pt);
                    }
                }
            }
        }
		
		//Tăng tốc quá trình, lọc những route nào đi qua 5 phường xã trở xuống xử lý trước, những route nào hơn thì ghi riêng ra xử lý sau
		if(related_wards.size() > 0) {
			//Khởi tạo mảng chứa
			ward_id = new int[related_wards.size()];
			
			for(int index = 0; index < related_wards.size(); index++) {
				Ward temp = related_wards.get(index);
				ward_id[index] = temp.getId();
				//System.out.println("ID: " + temp.getId() + " - " + temp.getName() + " - " + temp.getDistrict());
			}
			
			//System.out.println("Dataset: ");
			//int arr[] = {1, 2, 3, 4, 5}; 
			int r = 3; 
			int n = ward_id.length; 
			//Combination.printCombination(ward_id, n, r);
			
			dataset = Combination.generateCombination(ward_id, n, r);
			writeDataset(dataset);
			
//			for(Dataset data : dataset) {
//				System.out.println(data.toString());
//			}
			
			//Xoa bien
			dataset.removeAll(dataset);
			related_wards.removeAll(related_wards);
		}
		else
		{
			System.out.println("Đi qua hơn 5 phường, xã => bỏ qua!");
			writeRouteOver15Wards(route);
			related_wards.removeAll(related_wards);
			
		}	
	}
	
	 /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void writeRouteOver15Wards(String route) {
    	 FileWriter fileWriter = null;
    	 try {
             fileWriter = new FileWriter("C:\\Users\\Administrator\\Desktop\\data\\route_over_15_wards", true);  
             // Write a new Country object list to the CSV file
             fileWriter.append(route);
             fileWriter.append("\n");
  
         } catch (Exception e) {
             System.out.println("Error in File Writer !!!");
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 System.out.println("Error while flushing/closing fileWriter !!!");
                 e.printStackTrace();
             }
         }
    }
    
	
	 /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void writeDataset(List<Dataset> dataset) {
    	 FileWriter fileWriter = null;
    	 int numberOfVaild = 0;
    	 int numberOfExisted = 0;
    	 try {
             fileWriter = new FileWriter("C:\\Users\\Administrator\\Desktop\\data\\dataset", true);  
             // Write a new Country object list to the CSV file
             for (Dataset pt : dataset) {
            	 boolean isExisted = false;
            	 
            	 outerloop:
            	 for(Dataset existedDataset : finalDataset) {
            		 if(existedDataset.getX().compareToIgnoreCase(pt.getX()) == 0 &&
            				 existedDataset.getY().compareToIgnoreCase(pt.getY()) == 0 &&
            						 existedDataset.getZ().compareToIgnoreCase(pt.getZ()) == 0)
            			 {
            			 	//Đánh dấu dataset này đã tồn tại
            			 	isExisted = true;
            			 	break outerloop;
            			 }
            		
            	 }
            	 if(!isExisted) {
            		 finalDataset.add(pt);
            		 fileWriter.append(pt.toString());
            		 fileWriter.append("\n");
            		 numberOfVaild++;
            	 }
            	 else {
            		 existDataset.add(pt);
            		 numberOfExisted++;
            	 }
            	 
             }
             System.out.println("Tổng số: " + dataset.size());
             System.out.println("Không bị trùng: " + numberOfVaild);
             System.out.println("Bị trùng: " + numberOfExisted);
         } catch (Exception e) {
             System.out.println("Error in File Writer !!!");
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 System.out.println("Error while flushing/closing fileWriter !!!");
                 e.printStackTrace();
             }
         }
    }
    
    /**
     * Chuyển từ millisecond sang hh:mm:ss
     * @param millis
     * @return
     */
    public static String convertTime(long millis) {
    	return String.format("%02d:%02d:%02d", 
    		    TimeUnit.MILLISECONDS.toHours(millis),
    		    TimeUnit.MILLISECONDS.toMinutes(millis) - 
    		    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
    		    TimeUnit.MILLISECONDS.toSeconds(millis) - 
    		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }
    
    /**
     * Đọc tệp route đã được lọc (Những route đi qua ít nhất 3 phường, xã)
     * @return
     */
    public static List<String> readFinalRouteData(){
		BufferedReader br = null;
		List<String> routes = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data\\route_over_15_wards"));
            
            while ((line = br.readLine()) != null){
                if(!line.contains("encode")) {
                	//Đây là một route
                	routes.add(line.trim());
                }
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Number Of Routes: " + routes.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return routes;
	}
    
    /**
     * Đọc tệp dataset đã ghi trước đó
     * @return
     */
    public static List<Dataset> readExistedDataset(){
		BufferedReader br = null;
		List<Dataset> datasets = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data\\dataset"));
            
            while ((line = br.readLine()) != null){
            	Dataset temp =  new Dataset();
            	String[] parts = line.split(" ");
            	temp.setPrefix(parts[0]);
            	temp.setX((parts[1].split(":")[1]));
            	temp.setY((parts[2].split(":")[1]));
            	temp.setZ((parts[3].split(":")[1]));
            	datasets.add(temp);
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Datasets: " + datasets.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return datasets;
	}
	
    
    
	public static void main (String[] args) { 
		//Đọc danh sách xã phường
		wards = WardReader.readWardData();
		routes = readFinalRouteData();
		
		//Đọc dataset đã tồn tại nếu có
		finalDataset = readExistedDataset();
		
		cal = Calendar.getInstance();//Khai báo thời gian
		beginTimestamp = System.currentTimeMillis(); //Lấy thời gian bắt đầu
		
		for(int i = 0; i < 1 ; i++) {
			
			double rate = ((double)i / (double) routes.size()) * 100;
			long endTimestamp = System.currentTimeMillis();
			diffTimestamp =  endTimestamp - beginTimestamp;
			
			System.out.println("--------------- Tiến độ: " + (double) (Math.round(rate * 100.0) / 100.0) + "% -----------------------------");
			System.out.println("--------------- Thời gian trôi qua: " + convertTime(diffTimestamp) + "----------------");
			System.out.println("Hiện tại: " + (i + 1) + "/" + routes.size());
			handler(routes.get(i));
		}
		
//		
//		for(String route : routes) {
//			handler(route);
//		}
//		
		
//		String route = "m|faAe|tiSkH~DoBjAeCxAuR~LcEdCgN|Ied@hY_CvA[NcAn@iBpAgC|Akc@xXkBjAyA~@eAz@sBhAqCbByNbJgDlBwDfCmHtE{JfG{U~NyA|@aHnEwCdBiUrN";
//		System.out.println("Lộ trình: " + route);
//		System.out.println("Đi qua: ");
//		for (Ward pt : wards){
//            //Chuyển ward từ chuỗi poly thành List<LatLng>
//            List<LatLng> wardPath = PolyUtils.decode(pt.getEncodedpoly());
//            
//            //Duyệt tất cả điểm của chuỗi poly xem có nằm trong ward hay không?
//            for (LatLng point : PolyUtils.decode(route)){
//                if(PolyUtils.containsLocation(point, wardPath, false)){
//                    //Nếu đúng - Điểm nằm trong ward => Route đi qua ward
//                    if(!related_wards.contains(pt)){
//                    	//Chỉ thêm vào một lần duy nhất
//                    	related_wards.add(pt);
//                    }
//                }
//            }
//        }
//		
//		//Khởi tạo mảng chứa
//		ward_id = new int[related_wards.size()];
//		
//		for(int index = 0; index < related_wards.size(); index++) {
//			Ward temp = related_wards.get(index);
//			ward_id[index] = temp.getId();
//			System.out.println("ID: " + temp.getId() + " - " + temp.getName() + " - " + temp.getDistrict());
//		}
//		
//		System.out.println("Dataset: ");
//		//int arr[] = {1, 2, 3, 4, 5}; 
//		int r = 3; 
//		int n = ward_id.length; 
//		//Combination.printCombination(ward_id, n, r);
//		
//		dataset = Combination.generateCombination(ward_id, n, r);
//		
//		for(Dataset data : dataset) {
//			System.out.println(data.toString());
//		}
//		
	} 
	
}
