package Handler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import Classes.Dataset;
import Classes.Frequency;
import Classes.FrequencyDataset;
import Classes.LatLng;
import Classes.Ward;
import Utils.Combination;
import Utils.PolyUtils;
import Utils.TimKiem;

public class FilterDataset {

	//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<Ward> wards  = new ArrayList<>();
		
		//Danh sách các ward mà route hiện tại đi qua
		public static List<Ward> related_wards = new ArrayList<>();
		
		//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<String> routes  = new ArrayList<>();
		
		//Danh sách các tổng cộng các xã phường đọc được từ tệp
		public static List<Dataset> dataset  = new ArrayList<>();
		
		//Chứa dataset tổng hợp được từ tất cả các route
		public static List<FrequencyDataset> finalDataset = new ArrayList<>();
		
		//Chứa những dataset đã tồn tại ít nhất 1 lần trong finalDataset
		public static List<Dataset> existDataset = new ArrayList<>();
		
		public static int[] ward_id;
		
		public static long beginTimestamp = 0;
		
		public static long diffTimestamp = 0;
		
		public static Calendar cal;
		
	/*Driver function to check for above function*/
		
	public static void handler(String route) {
		System.out.println("Lộ trình: " + route);
		//System.out.println("Đi qua: ");
		for (Ward pt : wards){
            //Chuyển ward từ chuỗi poly thành List<LatLng>
            List<LatLng> wardPath = PolyUtils.decode(pt.getEncodedpoly());
            
            //Duyệt tất cả điểm của chuỗi poly xem có nằm trong ward hay không?
            for (LatLng point : PolyUtils.decode(route)){
                if(PolyUtils.containsLocation(point, wardPath, false)){
                    //Nếu đúng - Điểm nằm trong ward => Route đi qua ward
                    if(!related_wards.contains(pt)){
                    	//Chỉ thêm vào một lần duy nhất
                    	related_wards.add(pt);
                    }
                }
            }
        }
		
		//Tăng tốc quá trình, lọc những route nào đi qua 5 phường xã trở xuống xử lý trước, những route nào hơn thì ghi riêng ra xử lý sau
		if(related_wards.size() >= 0) {
			//Khởi tạo mảng chứa
			ward_id = new int[related_wards.size()];
			
			for(int index = 0; index < related_wards.size(); index++) {
				Ward temp = related_wards.get(index);
				ward_id[index] = temp.getId();
				//System.out.println("ID: " + temp.getId() + " - " + temp.getName() + " - " + temp.getDistrict());
			}
			
			//System.out.println("Dataset: ");
			//int arr[] = {1, 2, 3, 4, 5}; 
			int r = 3; 
			int n = ward_id.length; 
			//Combination.printCombination(ward_id, n, r);
			
			dataset = Combination.generateCombination(ward_id, n, r);
			handleDataset(dataset);
			
//			for(Dataset data : dataset) {
//				System.out.println(data.toString());
//			}
//			
			//Xoa bien
			dataset.removeAll(dataset);
			related_wards.removeAll(related_wards);
		}
		else
		{
			System.out.println("Đi qua hơn 15 phường, xã => bỏ qua!");
			writeRouteOver15Wards(route);
			
			dataset.removeAll(dataset);
			related_wards.removeAll(related_wards);
		}	
	}
	
	 /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void writeRouteOver15Wards(String route) {
    	 FileWriter fileWriter = null;
    	 try {
             fileWriter = new FileWriter("data\\route_over_20_wards", true);  
             // Write a new Country object list to the CSV file
             fileWriter.append(route);
             fileWriter.append("\n");
  
         } catch (Exception e) {
             System.out.println("Error in File Writer !!!");
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 System.out.println("Error while flushing/closing fileWriter !!!");
                 e.printStackTrace();
             }
         }
    }
    
	
	 /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void handleDataset(List<Dataset> dataset) {
    	 for (Dataset pt : dataset) {
        	 boolean isExisted = false;
        	 
        	 outerloop:
        	 for(FrequencyDataset existedDataset : finalDataset) {
        		 if(existedDataset.getDataset().getX().compareToIgnoreCase(pt.getX()) == 0 &&
        				 existedDataset.getDataset().getY().compareToIgnoreCase(pt.getY()) == 0 &&
        						 existedDataset.getDataset().getZ().compareToIgnoreCase(pt.getZ()) == 0)
        			 {
        			 	//Đánh dấu dataset này đã tồn tại
        			 	isExisted = true;
        			 	existedDataset.setTimes(existedDataset.getTimes() + 1);
        			 	break outerloop;
        			 }
        		
        	 }
        	 if(!isExisted) {
        		 finalDataset.add(new FrequencyDataset(pt));
        	 }        	 
         }	
    	 
    }
    
    
    /***
     * Ghi tệp mỗi 100 bản ghi
     */
	public static void 	writeFilterDataset() {
		 //Sắp xếp
   	 	Collections.sort(finalDataset, new Comparator<FrequencyDataset>() {
				public int compare(FrequencyDataset f1, FrequencyDataset f2) {
    			if(f1.getTimes() < f2.getTimes())
    				return 1;
    			else if (f1.getTimes() > f2.getTimes())
    				return -1;
    			else return 0;
    		}
		});
   	 
		//Ghi xuống tệp
		FileWriter fileWriter = null;
    	 try {
             fileWriter = new FileWriter("data\\Dataset_Filter");  
             // Write a new Country object list to the CSV file
             for(int m = 0; m < finalDataset.size(); m ++) {
            	 fileWriter.append(finalDataset.get(m).toString());
	             fileWriter.append("\n");
 			}
           
         } catch (Exception e) {
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
	}
    
    /**
     * Chuyển từ millisecond sang hh:mm:ss
     * @param millis
     * @return
     */
    public static String convertTime(long millis) {
    	return String.format("%02d:%02d:%02d", 
    		    TimeUnit.MILLISECONDS.toHours(millis),
    		    TimeUnit.MILLISECONDS.toMinutes(millis) - 
    		    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
    		    TimeUnit.MILLISECONDS.toSeconds(millis) - 
    		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }
    
    /**
     * Đọc tệp route đã được lọc (Những route đi qua ít nhất 3 phường, xã)
     * @return
     */
    public static List<String> readFinalRouteData(){
		BufferedReader br = null;
		List<String> routes = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("data\\route_over_20_wards"));
            
            while ((line = br.readLine()) != null){
                if(!line.contains("encode")) {
                	//Đây là một route
                	routes.add(line.trim());
                }
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Number Of Routes: " + routes.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return routes;
	}
    
    /**
     * Đọc tệp dataset đã ghi trước đó
     * @return
     */
    public static List<FrequencyDataset> readExistedDataset(){
		BufferedReader br = null;
		List<FrequencyDataset> datasets = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("data\\dataset_filter"));
            
            while ((line = br.readLine()) != null){
            	FrequencyDataset fd = new FrequencyDataset();
            	Dataset dataset =  new Dataset();
            	String[] parts = line.split(" ");
            	dataset.setPrefix(parts[0]);
            	dataset.setX((parts[1].split(":")[1]));
            	dataset.setY((parts[2].split(":")[1]));
            	dataset.setZ((parts[3].split(":")[1]));
            	
            	fd.setDataset(dataset);
            	fd.setTimes(Integer.parseInt(parts[4]));
            	datasets.add(fd);
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Datasets: " + datasets.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return datasets;
	}
	
    
	public static void main (String[] args) { 
		//Đọc danh sách xã phường
		wards = WardReader.readWardData();
		routes = readFinalRouteData();
		
		//Đọc dataset đã tồn tại nếu có
		finalDataset = readExistedDataset();
		
		cal = Calendar.getInstance();//Khai báo thời gian
		beginTimestamp = System.currentTimeMillis(); //Lấy thời gian bắt đầu
//		handler("}f}`AqbjjSr@FPeBR_BZiBj@wAd@qAb@sAj@wAVX~@r@fAl@rAn@`B~@xAt@dBr@dBl@t@J????t@B^u@n@oAf@cAd@{@b@gA\\y@b@{@\\mANqADqA?sAH{AGcBMwASaBQgBSeBSgBUmBU_BQaBSkBUuBSuBSyBYaCQ{BYsBSqBc@yAYuBPiBfAm@z@Vv@j@f@nA\\`BPnB`BRpALfANhANdAHrANzAFdBLrAPrALxALtARfBPvALtBPBc@cAGaAI_AIgAKXy@^_A^}@");
		for(int i = 3702; i < routes.size() ; i++) {
			
			double rate = ((double)i / (double) routes.size()) * 100;
			long endTimestamp = System.currentTimeMillis();
			diffTimestamp =  endTimestamp - beginTimestamp;
			
			System.out.println("--------------- Tiến độ: " + (double) (Math.round(rate * 100.0) / 100.0) + "% -----------------------------");
			System.out.println("--------------- Thời gian trôi qua: " + convertTime(diffTimestamp) + "----------------");
			System.out.println("Hiện tại: " + (i + 1) + "/" + routes.size());
			System.out.println("Dữ liệu: " + finalDataset.size());
			handler(routes.get(i));
			
			//Cứ 5000 dòng bản ghi thì ghi tệp 1 lần
			if(i % 50 == 0 || i == routes.size() - 1) {
				writeFilterDataset();
			}
		}
	} 
	
}
