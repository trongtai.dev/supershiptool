package Handler;

import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import Classes.LatLng;
import Classes.Event;
import Classes.SimpleEvent;
import Utils.PolyUtils;


public class DataTransportId {
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String COMMA_DELIMITER = ","; // Split by comma
	private static final long DIFF_TIME = 300000; // Thời gian giãn cách 5 phút, quá 5 phút xem như là một chu trình mới
	private static final long MIN_EVENTS = 10; //Chỉ lấy những đoạn trên 10 điểm liên tục
	
	private static List<String> transportIds = new ArrayList();
	private static List<SimpleEvent> events = new ArrayList();
	private static List<String> encodedPaths = new ArrayList();
	
    public static void main(String[] args) {
    	transportIds = readTransportIdFromFile();//1110 transportId
    	
    	for(int k = 424; k < transportIds.size() ; k ++) {
    		events = readEventsOfTransportId(transportIds.get(k));
    		encodedPaths.add("encode " + transportIds.get(k));//Dòng đánh dấu
    		System.out.println("---------------------------- Lộ trình thứ "+ k +"/" + transportIds.size() +": "+  transportIds.get(k) + "----------------------------");
    		
    		//Sau khi đọc xong sẽ có danh sách events
        	//Sort event theo Timestamp tăng dần
        	Collections.sort(events, new Comparator<SimpleEvent>() {
        		public int compare(SimpleEvent ev1, SimpleEvent ev2) {
        			if(parseStringToMiliseconds(ev1.getTIMESTAMP()) > parseStringToMiliseconds(ev2.getTIMESTAMP()))
        				return 1;
        			else if (parseStringToMiliseconds(ev1.getTIMESTAMP()) < parseStringToMiliseconds(ev2.getTIMESTAMP()))
        				return -1;
        			else return 0;
        		}
    		});
        	//printEvents(events.subList(0, 500));
        	//System.out.println("---------------------------");
    		//Tại đây là đã sắp xếp xong tăng dần theo thời gian
        	int preIndex = 0;
        	for(int i = 0; i < events.size() - 1; i++) {
        		if(parseStringToMiliseconds(events.get(i + 1).getTIMESTAMP()) - parseStringToMiliseconds(events.get(i).getTIMESTAMP()) > DIFF_TIME) {
        			//Chênh lệch thời gian hơn ngưỡng-> cắt thành từng khúc
        			//printEvents(events.subList(preIndex, i)); //Hiển thị từng khúc
        			
        			//System.out.println("i: " + events.get(i).getEvent_id() + "=>" + parseStringToTime(events.get(i).getTIMESTAMP()).getTime());
        			//System.out.println("i + 1: " + events.get(i + 1).getEvent_id() + "=>" + parseStringToTime(events.get(i + 1).getTIMESTAMP()).getTime());
        			//System.out.println("diff: " + (parseStringToTime(events.get(i + 1).getTIMESTAMP()).getTime() - parseStringToTime(events.get(i).getTIMESTAMP()).getTime()));
        	    	
        			if(events.subList(preIndex, i).size() > MIN_EVENTS) {//Chỉ lấy những đoạn nào trên 10 điểm liên tục
        				List<LatLng> pathInLatLng = PolyUtils.simplify(parseEventsToLatLng(events.subList(preIndex, i)), 0.7);
        	    	    
            	    	String encodePath = PolyUtils.encode(pathInLatLng);
            	    	encodedPaths.add(encodePath);
            	    	
            	    	//System.out.println("Encode Points: " + pathInLatLng.size());
            	    	System.out.println("Encode Path of " + transportIds.get(k) +": " + encodePath);
        			}  			
        			
        			//Cập nhật lại preIndex
        	    	preIndex = i + 1;
        		}
        	}    	
        	writeEncodedPathsToFile(encodedPaths);
        	
        	//TODO: Giải phóng, Xóa các biến
        	preIndex = 0;
        	events.removeAll(events);
        	encodedPaths.removeAll(encodedPaths);
    	}
    }
    
    /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void writeEncodedPathsToFile(List<String> encodePaths) {
    	 FileWriter fileWriter = null;
    	 try {
             fileWriter = new FileWriter("C:\\Users\\Administrator\\Desktop\\data\\encode_paths", true);  
             // Write a new Country object list to the CSV file
             for (String encodePath : encodePaths) {
                 fileWriter.append(encodePath);
                 fileWriter.append(NEW_LINE_SEPARATOR);
             }
             System.out.println("File was written successfully !!!");
  
         } catch (Exception e) {
             System.out.println("Error in File Writer !!!");
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 System.out.println("Error while flushing/closing fileWriter !!!");
                 e.printStackTrace();
             }
         }
    }
    
    /**
     * Đọc danh sách các TransportId từ file data gốc
     */
    public static List<String> getTransportId() {
    	 List<String> result = new ArrayList<String>();
    	 BufferedReader br = null;
         try {
             String line;
             br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data.csv"));
  
             // How to read file in java line by line?
             while ((line = br.readLine()) != null) {
            	 String[] splitData = line.split(COMMA_DELIMITER);
                 if(!result.contains((splitData[8].toString()))) {
                 	result.add(splitData[8]);
                 }
             }            
  
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 if (br != null)
                     br.close();
                 System.out.println("Number Of TransprortIds: " + transportIds.size());            
                 return result;
                 
             } catch (IOException crunchifyException) {
                 crunchifyException.printStackTrace();
             }
         }
         return null;
    }
    
    /**
     * Đọc danh sách các TransportId từ file có sẵn
     */
    public static List<String> readTransportIdFromFile() {
    	 List<String> result = new ArrayList<String>();
    	 BufferedReader br = null;
         try {
             String line;
             br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data\\transportId"));
  
             // How to read file in java line by line?
             while ((line = br.readLine()) != null) {
            	 result.add(line.trim());
             }            
  
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 if (br != null)
                     br.close();
                 System.out.println("Number Of TransprortIds: " + result.size());            
                 return result;
                 
             } catch (IOException crunchifyException) {
                 crunchifyException.printStackTrace();
             }
         }
         return null;
    }
    
    /**
     * Đọc các sự kiện của một hành trình (TransportId)
     * @param idToTest
     */
    private static List<SimpleEvent> readEventsOfTransportId(String idToTest) {
    	 List<SimpleEvent> result = new ArrayList<SimpleEvent>();
    	 BufferedReader br = null;
         try {
             String line;
             br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data.csv"));
  
             // How to read file in java line by line?
             while ((line = br.readLine()) != null) {
             	//parseCsvLineToEvent(idToTest, line);
           
            	 SimpleEvent temp = new SimpleEvent();
            	 String[] splitData = line.split(COMMA_DELIMITER);
                 for (int i = 0; i < splitData.length; i++) {
                  	if(splitData[8].toString().equalsIgnoreCase(idToTest) ) {
                  		
                      	temp.setEvent_id(splitData[0]);
                      	temp.setTIMESTAMP(splitData[2]);
                      	temp.setLatitude(splitData[4]);
                      	temp.setLongitude(splitData[5]);
                      	temp.setTransport_id(splitData[8]);
                      	
                      	if(!result.contains(temp))
                      		result.add(temp);
                  	}         
                  }            
             }             
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 if (br != null)
                     br.close();
                 System.out.println("Number events  Of TransprortIds " + idToTest + ": " + events.size());
                 return result;
                 
             } catch (IOException crunchifyException) {
                 crunchifyException.printStackTrace();
             }
         }
         return null;
	}
    
	
	public static List<LatLng> parseEventsToLatLng(List<SimpleEvent> data){
		List<LatLng> result = new ArrayList<>();
		for(SimpleEvent ev : data) {
			LatLng temp = new LatLng(Double.parseDouble(ev.getLatitude()), Double.parseDouble(ev.getLongitude()));
			result.add(temp);
		}
		return result;
	}
    
	/**
	 * Parse String to Time
	 * @param time
	 * @return
	 */
	public static long parseStringToMiliseconds(String time) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			Date temp = formatter.parse(time);
			return temp.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * In danh sách SimpleEvent cho trước
	 * @param temp
	 */
	public static void printEvents(List<SimpleEvent> temp) {
		for(SimpleEvent pt : temp) {
			System.out.println(pt.toString());
		}
	}
//    public static void writeToFile(String fileName, List<String> transportIds) {
//    	
//        FileWriter fileWriter = null;
//        try {
//            fileWriter = new FileWriter(fileName); 
//            // Write a new Country object list to the CSV file
//            for (String id : transportIds) {
//                fileWriter.append(id + "\n");
//            }
// 
//            System.out.println("TransportId file was created successfully !!!");
// 
//        } catch (Exception e) {
//            System.out.println("Error in CsvFileWriter !!!");
//            e.printStackTrace();
//        } finally {
//            try {
//                fileWriter.flush();
//                fileWriter.close();
//            } catch (IOException e) {
//                System.out.println("Error while flushing/closing fileWriter !!!");
//                e.printStackTrace();
//            }
//        }
//    }
}
