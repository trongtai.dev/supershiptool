package Handler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import Classes.Dataset;
import Classes.FrequencyDataset;
import Classes.LatLng;
import Classes.Ward;
import Utils.PolyUtils;

/**
 * 
 * @author Nguyen Trong Tai (trongtai.dev@gmail.com)
 * Tool để sinh ra dataset từ dữ liệu lọc được
 *
 */
public class GenerateDataset {
	//Chứa dataset tổng hợp được từ tất cả các route
	public static List<FrequencyDataset> finalFrequencyDataset = new ArrayList<>();
	
	//Chứa dataset tổng hợp được từ tất cả các route
	public static List<Dataset> datasetWithoutFrequency = new ArrayList<>();
	
	//Danh sách các tổng cộng các xã phường đọc được từ tệp
	public static List<String> ward_ids  = new ArrayList<>();
	
	public static List<String> top_ward_ids  = new ArrayList<>();

	public static long beginTimestamp = 0;
	
	public static long diffTimestamp = 0;
	
	public static Calendar cal;
	
	public static Random rand;
	public static List<Integer> indexed = new ArrayList<>();
	
	/**
	 * Đọc danh sách xã phường từ file Data
	 * @return
	 */
	public static List<String> readWardId(){
		BufferedReader br = null;
		List<String> ward_ids = new ArrayList<>();
        try {
            String line;
            br = new BufferedReader(new FileReader("data\\ward_id"));            
            while ((line = br.readLine()) != null){
            	ward_ids.add(line.trim());
            }  
 
       } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
        return ward_ids;
	}
	
	/**
	 * Đọc danh sách xã phường từ file Data
	 * @return
	 */
	public static List<String> readTopWardId(){
		BufferedReader br = null;
		List<String> ward_ids = new ArrayList<>();
        try {
            String line;
            br = new BufferedReader(new FileReader("data\\top_ward_id"));            
            while ((line = br.readLine()) != null){
            	ward_ids.add(line.trim());
            }  
 
       } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
        return ward_ids;
	}
	
	
	/**
	 * Đọc dataset thô
	 * @return
	 */
	public static List<FrequencyDataset> readExistedDataset(){
		BufferedReader br = null;
		List<FrequencyDataset> datasets = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("data\\dataset_filter"));
            
            while ((line = br.readLine()) != null){
            	FrequencyDataset fd = new FrequencyDataset();
            	Dataset dataset =  new Dataset();
            	String[] parts = line.split(" ");
            	dataset.setPrefix(parts[0]);
            	dataset.setX((parts[1].split(":")[1]));
            	dataset.setY((parts[2].split(":")[1]));
            	dataset.setZ((parts[3].split(":")[1]));
            	
            	fd.setDataset(dataset);
            	fd.setTimes(Integer.parseInt(parts[4]));
            	datasets.add(fd);
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Datasets: " + datasets.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return datasets;
	}
	

	/**
	 * Đọc dataset thô
	 * @return
	 */
	public static List<Dataset> readExistedDatasetWithoutFrequency(){
		BufferedReader br = null;
		List<Dataset> datasets = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("data\\data.t.3.txt"));
            
            while ((line = br.readLine()) != null){
            	Dataset dataset =  new Dataset();
            	String[] parts = line.split(" ");
            	dataset.setPrefix(parts[0]);
            	dataset.setX((parts[1].split(":")[1]));
            	dataset.setY((parts[2].split(":")[1]));
            	dataset.setZ((parts[3].split(":")[1]));
            	datasets.add(dataset);
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Datasets: " + datasets.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return datasets;
	}
	
	 /**
     * Chuyển từ millisecond sang hh:mm:ss
     * @param millis
     * @return
     */
    public static String convertTime(long millis) {
    	return String.format("%02d:%02d:%02d", 
    		    TimeUnit.MILLISECONDS.toHours(millis),
    		    TimeUnit.MILLISECONDS.toMinutes(millis) - 
    		    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
    		    TimeUnit.MILLISECONDS.toSeconds(millis) - 
    		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }
    
    /**
     * Trả về 1 dataset có thể train được từ dataset thô
     * P1: 322 cols
     * P2: 322 cols
     * P3: 322 cols
     * @param dataset
     */
    public static void handler(FrequencyDataset data) {
    	String p1 = data.getDataset().getX();
    	String p2 = data.getDataset().getY();
    	String p3 = data.getDataset().getZ();
    	
    	//String dataset = data.getDataset().getPrefix() + " ";
    	String dataset = "+1 ";
    	
    	int index = 0;
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P1
    		if(ward_ids.get(i).compareToIgnoreCase(p1) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":-1 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P2
    		if(ward_ids.get(i).compareToIgnoreCase(p2) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":-1 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P3
    		if(ward_ids.get(i).compareToIgnoreCase(p3) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":-1 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	writeFinalDataset(dataset);
    }
    
    public static void comnpactHandler(FrequencyDataset data) {
    	String p1 = data.getDataset().getX();
    	String p2 = data.getDataset().getY();
    	String p3 = data.getDataset().getZ();
    	
    	//String dataset = data.getDataset().getPrefix() + " ";
    	String dataset = "-1 ";
    	int index = 0;
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P1
    		if(ward_ids.get(i).compareToIgnoreCase(p1) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
//    			String datasetElement = (index + 1) + ":-1 ";
//    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P2
    		if(ward_ids.get(i).compareToIgnoreCase(p2) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
//    			String datasetElement = (index + 1) + ":-1 ";
//    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P3
    		if(ward_ids.get(i).compareToIgnoreCase(p3) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
//    			String datasetElement = (index + 1) + ":-1 ";
//    			dataset += datasetElement;
    			index++;
    		}
    	}
    	writeFinalDataset(dataset);
    }
    
    /**
     * Trả về 1 dataset có thể train được từ dataset thô
     * P1: 255 cols: Trong đó 254 cols (top_ward_ids.size())là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * P2: 255 cols: Trong đó 254 cols là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * P3: 255 cols: Trong đó 254 cols là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * @param dataset
     */
    public static void handlerVersion2(FrequencyDataset data) {
    	String p1 = data.getDataset().getX();
    	String p2 = data.getDataset().getY();
    	String p3 = data.getDataset().getZ();
    	
    	String dataset = "+1 ";
    	int index = 0;
    	int zeroElementTimes = 0;
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P1
    		if(top_ward_ids.get(i).compareToIgnoreCase(p1) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P2
    		if(top_ward_ids.get(i).compareToIgnoreCase(p2) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P3
    		if(top_ward_ids.get(i).compareToIgnoreCase(p3) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	writeFinalDatasetVersion2(dataset);
    }
    
    /**
     * Trả về 1 dataset có thể train được từ dataset thô để TEST
     * P1: 255 cols: Trong đó 254 cols (top_ward_ids.size())là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * P2: 255 cols: Trong đó 254 cols là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * P3: 255 cols: Trong đó 254 cols là top các phường xã, col thứ 255 là col chung cho 68 phường xã còn lại
     * @param dataset
     */
    public static void handlerVersion3(Dataset data) {
    	String p1 = data.getX();
    	String p2 = data.getY();
    	String p3 = data.getZ();
    	
    	String dataset = "+1 ";
    	int index = 0;
    	int zeroElementTimes = 0;
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P1
    		if(top_ward_ids.get(i).compareToIgnoreCase(p1) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P2
    		if(top_ward_ids.get(i).compareToIgnoreCase(p2) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	
    	for(int i = 0; i < top_ward_ids.size(); i++) {
    		//Xử lý P3
    		if(top_ward_ids.get(i).compareToIgnoreCase(p3) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    			zeroElementTimes++;
    		}
    	}
    	if(zeroElementTimes == top_ward_ids.size()) {
    		//Nằm ngoài Top 254 các phường xã -> nó thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":1 ";
			dataset += datasetElement;
			index++;
    	}
    	else {
    		//Nằm trong Top 254 các phường xã -> nó không thuộc cột thứ 255
    		String datasetElement = (index + 1) + ":0 ";
			dataset += datasetElement;
			index++;
    	}
    	zeroElementTimes = 0;
    	
    	writeFinalDatasetVersion2(dataset);
    }
    
    /**
     * Trả về 1 dataset có thể train được từ dataset thô
     * P1: 322 cols
     * P2: 322 cols
     * P3: 322 cols
     * @param dataset
     */
    public static void handlerVersion4(Dataset data) {
    	String p1 = data.getX();
    	String p2 = data.getY();
    	String p3 = data.getZ();
    	
    	String dataset = "+1 ";
    	int index = 0;
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P1
    		if(ward_ids.get(i).compareToIgnoreCase(p1) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P2
    		if(ward_ids.get(i).compareToIgnoreCase(p2) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	
    	for(int i = 0; i < 322; i++) {
    		//Xử lý P3
    		if(ward_ids.get(i).compareToIgnoreCase(p3) == 0) {
    			String datasetElement = (index + 1) + ":1 ";
    			dataset += datasetElement;
    			index++;
    		}
    		else {
    			String datasetElement = (index + 1) + ":0 ";
    			dataset += datasetElement;
    			index++;
    		}
    	}
    	writeFinalDataset(dataset);
    }
    
    /**
     * Ghi kết quả xuống tệp
     * @param dataset
     */
	public static void writeFinalDataset(String dataset) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter("data\\data.t.5", true);
			// Write a new Country object list to the CSV file
			fileWriter.append(dataset);
			fileWriter.append("\n");

		} catch (Exception e) {
			System.out.println("Error in File Writer !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}
	
	/**
     * Ghi kết quả xuống tệp
     * @param dataset
     */
	public static void writeFinalDatasetVersion2(String dataset) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter("data\\dataset_test", true);
			// Write a new Country object list to the CSV file
			fileWriter.append(dataset);
			fileWriter.append("\n");

		} catch (Exception e) {
			System.out.println("Error in File Writer !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}
    
	 
	
	public static void main(String argn[]) {
		//Đọc ward id
		ward_ids = readWardId();
		top_ward_ids = readTopWardId();
		rand = new Random();
//		System.out.println(top_ward_ids.size());
//		
//		FrequencyDataset test = new FrequencyDataset();
//		Dataset temp = new Dataset();
//		temp.setX("26920");
//		temp.setY("26956");
//		temp.setZ("27094");
//		test.setDataset(temp);
//		handler(test);
		
		finalFrequencyDataset = readExistedDataset();
//		datasetWithoutFrequency = readExistedDatasetWithoutFrequency();
		cal = Calendar.getInstance();//Khai báo thời gian
		beginTimestamp = System.currentTimeMillis(); //Lấy thời gian bắt đầu
		
		//- 90: 93521
		//100 : 85134
		//sum: 1452670
		while(indexed.size() < 1000) {
			int index = rand.nextInt(85134 - 0) + 0;
			if(!indexed.contains(index)) {
				indexed.add(index);
				handler(finalFrequencyDataset.get(index));
			}
		}
//		for(int i = 0; i < 10000 ; i++) {//85134
//			
//			
//			double rate = ((double)i / (double) finalFrequencyDataset.size()) * 100;
//			long endTimestamp = System.currentTimeMillis();
//			diffTimestamp =  endTimestamp - beginTimestamp;
//			
//			System.out.println("--------------- Tiến độ: " + (double) (Math.round(rate * 100.0) / 100.0) + "% -----------------------------");
//			System.out.println("--------------- Thời gian trôi qua: " + convertTime(diffTimestamp) + "----------------");
//			System.out.println("Hiện tại: " + (i + 1) + "/" + finalFrequencyDataset.size());
//			//handler(finalFrequencyDataset.get(i));
//			comnpactHandler(finalFrequencyDataset.get(i));
//			
//			
//		}	
	}
}
