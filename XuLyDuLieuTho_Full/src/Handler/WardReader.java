package Handler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Classes.*;
import Utils.*;

public class WardReader {
	//Danh sách các tổng cộng các xã phường đọc được từ tệp
	public static List<Ward> wards  = new ArrayList<>();
	
	//Danh sách các ward mà route hiện tại đi qua
	public static List<Ward> related_wards = new ArrayList<>();
	
	//Danh sách các tổng cộng các xã phường đọc được từ tệp
	public static List<String> routes  = new ArrayList<>();
		
	
	
	public static void main(String args[]) {
		//Đọc danh sách xã phường
		wards = readWardData();
		
		//Đọc danh sách các route
		routes = readRouteData();
		
		for (String route : routes) {
			for (Ward pt : wards){
	            //Chuyển ward từ chuỗi poly thành List<LatLng>
	            List<LatLng> wardPath = PolyUtils.decode(pt.getEncodedpoly());
	            
	            //Duyệt tất cả điểm của chuỗi poly xem có nằm trong ward hay không?
	            for (LatLng point : PolyUtils.decode(route)){
	                if(PolyUtils.containsLocation(point, wardPath, false)){
	                    //Nếu đúng - Điểm nằm trong ward => Route đi qua ward
	                    if(!related_wards.contains(pt)){
	                    	//Chỉ thêm vào một lần duy nhất
	                    	related_wards.add(pt);
	                    }
	                }
	            }
	        }
			if(related_wards.size() >= 3) {
				//Chỉ lấy những route nào đi qua từ 3 ward trở lên
				List<String> result = new ArrayList<>();
				
//				String pathOver = "Đi qua: ";
//				for(Ward ward : related_wards) {
//					if(related_wards.get(related_wards.size() - 1).getName().equalsIgnoreCase(ward.getName())) {
//						pathOver = pathOver + ward.getName() + " - " + ward.getDistrict();
//					}
//					else 
//						pathOver = pathOver + ward.getName() + " - " + ward.getDistrict() + ", ";		
//				}
				
				result.add(route);
//				result.add(pathOver);
//				result.add("---------------------------------------------------------");
				//Ghi xuống file
				writeRouteOver3Wards(result);
				
				//Reset các biến
				related_wards.removeAll(related_wards);
				result.removeAll(result);
			}
				
		}
	}
	
	 /**
     * Ghi tệp dữ liệu đã được xử lý
     * @param filename
     */
    public static void writeRouteOver3Wards(List<String> result) {
    	 FileWriter fileWriter = null;
    	 try {
             fileWriter = new FileWriter("C:\\Users\\Administrator\\Desktop\\data\\route_final", true);  
             // Write a new Country object list to the CSV file
             for (String pt : result) {
                 fileWriter.append(pt);
                 fileWriter.append("\n");
             }
             System.out.println("File is being written !!!");
  
         } catch (Exception e) {
             System.out.println("Error in File Writer !!!");
             e.printStackTrace();
         } finally {
             try {
                 fileWriter.flush();
                 fileWriter.close();
             } catch (IOException e) {
                 System.out.println("Error while flushing/closing fileWriter !!!");
                 e.printStackTrace();
             }
         }
    }
	
	public static List<String> readRouteData(){
		BufferedReader br = null;
		List<String> routes = new ArrayList<>();
		try {
            String line;
            br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data\\encode_paths"));
            
            while ((line = br.readLine()) != null){
                if(!line.contains("encode")) {
                	//Đây là một route
                	routes.add(line.trim());
                }
            }  
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Number Of Routes: " + routes.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
		return routes;
	}
	
	
	/**
	 * Đọc danh sách xã phường từ file Data
	 * @return
	 */
	public static List<Ward> readWardData(){
		BufferedReader br = null;
		List<Ward> wards = new ArrayList<>();
        try {
            String line;
            br = new BufferedReader(new FileReader("data\\dulieuxaphuong.txt"));
 
            // How to read file in java line by line?
//            while ((line = br.readLine()) != null) {
//                events.add(parseCsvLine(line));
//            }
            
            String id = ""; //Id của phường/xã
            String name = ""; //Tên của phường/xã
            String district = ""; //Quận quản lý của phường/xã
            List<LatLng> temp = new ArrayList<>();      
            while ((line = br.readLine()) != null){
                if (line.contains("id")) id = line.split("\t\t")[1];
                else if (line.contains("name")) name = line.split("\t\t")[1];
                else if (line.contains("district")) district = line.split("\t\t")[1];
                else if (line.contains("encode"))
                {
                    String encodedPoly = PolyUtils.encode(temp);
//                    System.out.println("Mã phường/xã: " + id + "\n");
//                    System.out.println("Tên phường/xã: " + name + "\n");
//                    System.out.println("Quận: " + district + "\n");
//                    System.out.println("Encoded Poly: " + encodedPoly + "\n");
//                    System.out.println("-------------------------------------" + "\n");
                    //Thêm vào danh sách
                    Ward ward = new Ward();
                    ward.setId(Integer.parseInt(id));
                    ward.setName(name);
                    ward.setDistrict(district);
                    ward.setEncodedpoly(encodedPoly);
                    wards.add(ward);
                    //Xóa thông tin hiện tại
                    temp.removeAll(temp);
                }
                else {
                    String[] position = line.split(", ");
                    temp.add(new LatLng(Double.parseDouble(position[1]), Double.parseDouble(position[0])));
                }
            }  
 
       } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
//                System.out.println("Number Of Ward: " + wards.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
        return wards;
	}
}
