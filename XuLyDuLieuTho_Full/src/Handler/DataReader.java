package Handler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Classes.Event;
import Classes.SimpleEvent;

public class DataReader {
	private static final String COMMA_DELIMITER = ","; // Split by comma
	private static List<SimpleEvent> events = new ArrayList();
	
    public static void main(String[] args) {
 
        BufferedReader br = null;
        try {
            String line;
            br = new BufferedReader(new FileReader("C:\\Users\\Administrator\\Desktop\\data.csv"));
 
            // How to read file in java line by line?
            while ((line = br.readLine()) != null) {
                events.add(parseCsvLine(line));
            }
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                System.out.println("Number Of Events: " + events.size());
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }
    }
    
    
    public static SimpleEvent parseCsvLine(String csvLine) {
        List<String> result = new ArrayList<String>();
        SimpleEvent temp = new SimpleEvent();
  
        if (csvLine != null) {
            String[] splitData = csvLine.split(COMMA_DELIMITER);
            for (int i = 0; i < splitData.length; i++) {
                //result.add(splitData[i]);
            	temp.setEvent_id(splitData[0]);
            	temp.setTIMESTAMP(splitData[2]);
            	temp.setLatitude(splitData[4]);
            	temp.setLongitude(splitData[5]);
            	temp.setTransport_id(splitData[8]);
            }
        }
        return temp;
    }
}
